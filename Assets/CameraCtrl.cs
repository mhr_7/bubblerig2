﻿using UnityEngine;
using System.Collections;

public class CameraCtrl : MonoBehaviour {

	public int scroll, zoomSpeed;
	public Camera cam;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//transform.Translate(0,scroll*zoomSpeed,scroll*zoomSpeed,Space.World);
		cam.fieldOfView *= scroll*zoomSpeed;
		if(cam.orthographicSize > 3)
		cam.orthographicSize -= 1f;
	}
}
