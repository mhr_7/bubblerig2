﻿using UnityEngine;
using System.Collections;


public class TimeLimitScore : MonoBehaviour {

	public int scor, limNumb, waktu, limit, finalTime, timeStart, point = 0;
	public Transform[] explosion_pos;
	public GameObject tank, spawner, mainBack;
	public GameObject button_home, button_retry, wining, lose, Explode;
	public GameObject[] explos;
	public int fontSiz;
	public bool gameOver, win, once = false, bomb, over = false;
	private Vector2 mid, buttonLoc, exp_loc;

	int lim(){
		limNumb = Random.Range (10, 20);
		return limNumb;
	}

	void Start(){
		Time.timeScale = 1;
		timeStart = (int)Time.time;
		gameOver = win = bomb = false;
		tank = GameObject.FindGameObjectWithTag ("Tank");
		spawner = GameObject.FindGameObjectWithTag ("Respawn");
		fontSiz = tank.GetComponent<Tank> ().fontSize;
		limit = lim ();
		mid = buttonLoc = exp_loc = mainBack.transform.position;
		buttonLoc.y -= 6f;
	}

	void Update(){
		scor = tank.GetComponent<Tank> ().score;
		bomb = tank.GetComponent<Tank> ().lose;
		if (scor >= limit || bomb) {
			gameOver = true;
			point = 0;
			if(scor == limit){ win = true;
				if(waktu<=45) point = 5;
				else if(waktu>45 && waktu<=60) point = 3;
				else point=1;
			}
		}
		else waktu = (int)Time.time-timeStart;

		if (gameOver) {
			if(!once){
				finalTime = waktu;
				if(win){
					Instantiate(wining, mid, Quaternion.identity);
					button_show();
					over = true;
				}
				else {
					for(int i=0;i<explosion_pos.Length;i++){
						exploded(i);
					}
					Invoke("Loser", 3f);
					Invoke("button_show", 3f);
					//Invoke("explodeExit", 3f);
				}
				once = true;
			}
		}
	}

	void exploded(int i){
		exp_loc.x = explosion_pos[i].position.x;
		exp_loc.y = explosion_pos[i].position.y;
		Instantiate (Explode, exp_loc, Quaternion.identity);
		print ("meledak " + i);
	}

	void button_show(){
		buttonLoc.x+=4.3f;
		Instantiate(button_home, buttonLoc, Quaternion.identity);
		buttonLoc.x-=(4.3f*2f);
		Instantiate(button_retry, buttonLoc, Quaternion.identity);
		over = true;
	}

	void Loser(){
		Instantiate(lose, mid, Quaternion.identity);
	}

	void explodeExit(){
		for(int i=0; i<explos.Length; i++)
		Destroy (explos[i]);
	}

	/*void OnGUI(){
		GUI.depth = 0;
		GUI.skin.label.fontSize = fontSiz;
		if (over) {
			if(win){
				GUI.Label (new Rect (Screen.width *25/100, Screen.height * 25/100, 200, 60), "Congratulation !!!");
			}
			else{
				GUI.Label (new Rect (Screen.width *30/100, Screen.height * 25/100, 200, 60), "Game Over!!!");
			}
			GUI.Label (new Rect (Screen.width *28/100, Screen.height * 60/100, 200, 60), "Your Point is : " + finalTime);

		}/*else 
		{
			GUI.Label (new Rect (Screen.width *16/100, Screen.height *16/100, 100, 50), " " + limit);
			GUI.Label (new Rect (Screen.width *70/100, Screen.height *75/100, 100, 50), "Time :" + waktu);
		}
	}*/
}