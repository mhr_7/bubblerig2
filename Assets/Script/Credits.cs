﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Credits : MonoBehaviour {

	public Transform[] places;
	public GameObject credits_thing;
	public bool clicked;
	// Use this for initialization
	void Start () {
		clicked = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (clicked) {
			credits_thing.transform.position = places [0].transform.position;
		} else {
			credits_thing.transform.position = places [1].transform.position;
		}
	}

	void  OnMouseDown(){
		if (clicked) {
			clicked = false;
		} else {
			clicked = true;
		}
	}
}
