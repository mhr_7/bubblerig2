﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class score_text : MonoBehaviour {

	public int score;        // The player's score.

	Text text;                      // Reference to the Text component.
	public GameObject TimeLimitScore;
	
	
	void Awake ()
	{
		// Set up the reference.
		text = GetComponent <Text> ();

		// Reset the score.
		score = 0;
	}
	
	
	void Update ()
	{
		score = TimeLimitScore.GetComponent<TimeLimitScore>().scor;
		// Set the displayed text to be the word "Score" followed by the score value.
		text.text =""+score;
	}
}
