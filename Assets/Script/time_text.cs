﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class time_text : MonoBehaviour {

	private int time, timeStart;        // The player's score.
	public GameObject timeLimitScore;
	private bool over;
	Text text;                      // Reference to the Text component.

	void Start(){
		timeStart = (int)Time.time;
	}

	void Awake ()
	{
		// Set up the reference.
		text = GetComponent <Text> ();
	}
	
	
	void Update ()
	{
		over = timeLimitScore.GetComponent<TimeLimitScore>().gameOver;
		if(!over) time =(int)Time.time - timeStart;
		// Set the displayed text to be the word "Score" followed by the score value.
		text.text =""+time;
	}

}
