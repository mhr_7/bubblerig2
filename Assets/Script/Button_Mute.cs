﻿using UnityEngine;
using System.Collections;

public class Button_Mute : MonoBehaviour {

	public GameObject mainCam;
	public GameObject button_mute;
	public bool mute = false;
	void OnMouseDown(){
		if (!mute) {
//			if(button_mute) 
		//		Destroy(button_mute);
			button_mute = Resources.Load("mute") as GameObject;
			if(button_mute) print ("ada button");
			Instantiate(button_mute, transform.position, Quaternion.identity);
			AudioListener.pause = true;
//			AudioListener.volume = 0;
			mute = true;
		} else {
			Destroy(button_mute);
			button_mute = Resources.Load("mute_silang") as GameObject;
			Instantiate(button_mute, transform.position, Quaternion.identity);
			AudioListener.pause = false;
//			AudioListener.volume = 128;
			mute = false;
		}
	}
}
