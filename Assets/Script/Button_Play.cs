﻿using UnityEngine;
using System.Collections;

public class Button_Play : MonoBehaviour {

	public string Levels;
	public float TimeGap;
	public bool clicked;
	// Use this for initialization

	void Start(){
		clicked = false;
	}

	void Update(){
		if (clicked) {
			small ();
			Invoke ("goes", TimeGap);
			clicked = false;
		}
	}

	void  OnMouseDown(){
		clicked = true;
	}

	void small(){
		this.transform.localScale *= 0.7f;
		print ("Mengecil");
	}

	void goes(){
		Application.LoadLevel (Levels);
	}
}
