﻿using UnityEngine;
using System.Collections;

public class Spawn_snow : MonoBehaviour {

	public Transform place;
	public int timeGaps;
	public string snow_name;
	public GameObject[] snows;
	string[] snow_num = new string[10];
	// Use this for initialization
	void Start () {
		for (int i=0; i<snows.Length; i++) {
			snow_num[i] = snow_name+i;
		}
		StartCoroutine (spawner ());
	}

	Vector2 pos(){
		Vector2 nee;
		float ran = Random.Range (-7.53f, 7.53f);
		nee.y = place.transform.position.y;
		nee.x = place.transform.position.x + (float)ran;
		//nee.y = y;
		return nee;
	}
	
	GameObject aw(){
		int ran = Random.Range (0, snows.Length);
		return Resources.Load (snow_num [ran]) as GameObject;
	}
	
	public IEnumerator spawner(){
		while (true) {
			Instantiate(aw (), pos(), Quaternion.identity);
			yield return new WaitForSeconds(timeGaps);
		}
	}
}
