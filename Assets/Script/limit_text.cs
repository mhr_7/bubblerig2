﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class limit_text : MonoBehaviour {

	public int limit;        // The player's score.
	
	Text text;                      // Reference to the Text component.
	public GameObject TimeLimitScore;
	
	
	void Awake ()
	{
		// Set up the reference.
		text = GetComponent <Text> ();
		
		// Reset the score.
		limit = 0;
	}
	
	
	void Update ()
	{
		limit = TimeLimitScore.GetComponent<TimeLimitScore>().limit;
		// Set the displayed text to be the word "Score" followed by the score value.
		text.text =""+limit;
	}
}