﻿using UnityEngine;
using System.Collections;

public class ChangePipe : MonoBehaviour {

	public GameObject[] pipes;
	public Transform[] location;
	public bool swit, once;
	// Use this for initialization
	void Start () {
		swit = false;
		once = false;
		
		pipes[1].transform.position = location[0].position;
		pipes[0].transform.position = location[1].position;
	}
	
	// Update is called once per frame
	void Update () {
		if (once) 
		{
			if(swit){
				pipes[0].transform.position = location[0].position;
				pipes[1].transform.position = location[1].position;
/*			Destroy (pipes [0]);
			Instantiate (pipes [1], location[0].position, Quaternion.identity);
*/			once = false;
			} else {
				pipes[1].transform.position = location[0].position;
				pipes[0].transform.position = location[1].position;
			once = false;
			}
		}
	}

	void OnMouseDown(){
		if (swit)
			swit = false;
		else
			swit = true;

		once = true;
	}
}
