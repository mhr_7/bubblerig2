﻿using UnityEngine;
using System.Collections;

public class PlayMenu : MonoBehaviour {

	public GameObject menu, submenu, story;
	public Transform[] places;
	public int stat;
	public bool clicked;

	// Use this for initialization
	void Start () {
		clicked = false;
	}
	
	// Update is called once per frame
	void Update () {
		stat = story.GetComponent<Story> ().state;
		if (clicked ==true && stat>=4) {
			small();
			menu.transform.position = places[1].position;
			submenu.transform.position = places[0].position;
			clicked = false;
		}
	}

	void  OnMouseDown(){
		clicked = true;
	}

	void small(){
		this.transform.localScale *= 0.7f;
		print ("clicked");
	}
}
