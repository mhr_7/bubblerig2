﻿using UnityEngine;
using System.Collections;

public class spwanAwan : MonoBehaviour {

	public Transform place;
	public int timeGaps;
	public string awan_name;
	string[] awan_num = new string[10];
	// Use this for initialization
	void Start () {
		for (int i=0; i<3; i++) {
			awan_num[i] = awan_name+i;
		}
		StartCoroutine (spawner ());
	}

	int speed(){
		int cep = Random.Range (2, 5);
		return cep;
	}

	Vector2 pos(){
		Vector2 nee;
		float ran = Random.Range (-4f, 4f);
		nee.y = place.transform.position.y + (float)ran;
		nee.x = place.transform.position.x;
		//nee.y = y;
		return nee;
	}

	GameObject aw(){
		int ran = Random.Range (0, 3);
		return Resources.Load (awan_num [ran]) as GameObject;
	}

	public IEnumerator spawner(){
		while (true) {
			GameObject wan = Instantiate(aw (), pos(), Quaternion.identity) as GameObject;
			wan.GetComponent<awan>().speed = speed();
			//Rigidbody2D awan = Instantiate (aw(), pos (), Quaternion.identity) as Rigidbody2D;
			//awan.velocity = new Vector2 (4f, 0);
			yield return new WaitForSeconds(timeGaps);
		}
	}
}
