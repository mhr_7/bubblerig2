﻿using UnityEngine;
using System.Collections;

public class Story : MonoBehaviour {

	public Camera mainCam;
	public Transform[] CamPos, dialogPos, buttonPos;
	public float[] ortSize;
	public GameObject[] dialogs_pict;
	public GameObject buttonPict, storeRoom;
	private Animator storeRoomAnim;
	public int state, time, startTime;
	// Use this for initialization
	void Start () {
		state = 0;
		startTime = (int)Time.time;
		storeRoomAnim = storeRoom.GetComponent<Animator> ();
		storeRoomAnim.SetBool ("Gunc", false);
	}
	
	// Update is called once per frame
	void Update () {
		time = (int)Time.time - startTime;
		if (state == 0) { //someday
			if (time >= 2) {
				mainCam.transform.position = CamPos [1].position;
				mainCam.orthographicSize = ortSize [1];
				dialogs_pict [1].transform.position = dialogPos [1].transform.position; //someday
				buttonPict.transform.position = buttonPos[1].position;
			}
		} else if (state == 1) { //meledak
			storeRoomAnim.SetBool("Gunc", true);
			mainCam.transform.position = CamPos[2].position;

			dialogs_pict [0].transform.position = dialogPos [3].transform.position; //bomb
			dialogs_pict [1].transform.position = dialogPos [0].transform.position;
		} else if (state == 2) { //whoaa whats happend
			dialogs_pict [0].transform.position = dialogPos [0].transform.position; //keluar
			dialogs_pict [2].transform.position = dialogPos [2].transform.position; //masuk
		} else if (state == 3) { //it tooks time
			dialogs_pict [2].transform.position = dialogPos [0].transform.position;
			dialogs_pict [3].transform.position = dialogPos [2].transform.position;
		} else if (state == 4) { //we must control it
			dialogs_pict [3].transform.position = dialogPos [0].transform.position;
			dialogs_pict [4].transform.position = dialogPos [2].transform.position;
		} else if (state > 4) {
			mainCam.transform.position = CamPos[0].position;
			mainCam.orthographicSize = ortSize[0];
			dialogs_pict [4].transform.position = dialogPos [0].transform.position;
			buttonPict.transform.position = buttonPos[0].position;
		}
	}

	void OnMouseDown(){
		state++;
	}
}
